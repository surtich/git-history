import git from 'isomorphic-git';
import fs from 'fs';
import { fail, type Actions } from '@sveltejs/kit';

export const actions: Actions = {
	async commits({ request }) {
		const form = await request.formData();
		const path = form.get('path');
		const branch = form.get('branch');

		if (!path || path.length === 0) {
			return fail(404, {
				error: 'path is required!'
			});
		}

		if (!branch || branch.length === 0) {
			return fail(404, {
				error: 'branch is required!'
			});
		}

		let commits;
		let currentCommit;

		const commonProps = {
			fs,
			dir: path.toString()
		};
		try {
			commits = await git.log({
				...commonProps,
				ref: branch.toString()
			});
			currentCommit = await git.resolveRef({ ...commonProps, ref: 'HEAD' });
		} catch (error) {
			return fail(500, { error: error.message });
		}

		return { log_success: true, path, branch, commits: [...commits.reverse()], currentCommit };
	},

	async checkout({ request }) {
		const form = await request.formData();
		const hash = form.get('hash');
		const path = form.get('path');

		if (!path || path.length === 0) {
			return fail(404, {
				error: 'path is required!'
			});
		}

		if (!hash || hash.length === 0) {
			return fail(404, {
				error: 'hash is required!'
			});
		}

		try {
			await git.checkout({
				fs,
				dir: path.toString(),
				ref: hash.toString()
			});
		} catch (error) {
			return fail(500, { error: error.message });
		}
		return { checkout_success: true, currentCommit: hash };
	}
};
